<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
        $data = [];
        $post = $this->input->post();
        $this->load->helper('url');

        if (!empty($post)) {
            $this->load->model('jpush_model');
            $data['result'] = $this->jpush_model->test_push($post);
        }
        
		$this->load->view('welcome_message', $data);
	}

    public function push_example()
    {
        $data = [];
        $this->load->helper('url');

        if ($this->input->post('example')) {
            $this->load->model('jpush_model');
            $data['result'] = $this->jpush_model->push_example();
        }

        $this->load->view('push_example', $data);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */