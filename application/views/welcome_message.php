<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('/css/bootstrap.css') ?>">
    <script type="text/javascript" src="<?= base_url('/js/jquery-3.0.0.js') ?>"></script>
</head>
<body>
<br>
<div id="container" class="container">
    <div class="row">
        <div class="col-sm-2">
            <a class="btn btn-primary btn-block" href="<?= base_url('/') ?>">Push</a>
            <a class="btn btn-primary btn-block" href="<?= base_url('/welcome/push_example') ?>">Push Example</a>
        </div>
        <div class="col-sm-8">
            <form class="form-horizontal" role="form" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="">Notification Content</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="notification">您已成功收款200元</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="">Platform</label>
                    <div class="col-sm-10">
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox" name="platforms[]" value="android" checked="checked"> Android
                            </label>
                        </div>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox" name="platforms[]" value="ios"> IOS
                            </label>
                        </div>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox" name="platforms[]" value="winphone"> Windows Phone
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="">Audience</label>
                    <div class="col-sm-10">
                        <select id="audience" class="form-control" name="audience">
                            <option value="all">All</option>
                            <option value="filter">Filter</option>
                        </select>
                        <div id="audience-filter" class="hidden" style="margin-top:15px">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="">Tag</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="tag">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="">Alias</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="alias">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="">Registration ID</label>
                                <div class="col-sm-9">
                                    <input class="form-control" type="text" name="registration_id">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="">Document</label>
                    <div class="col-sm-10">
                        <a href="http://docs.jiguang.cn/server/rest_api_v3_push/">极光推送 Push API v3</a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button class="btn btn-success" type="submit">&nbsp;&nbsp; Push &nbsp;&nbsp;</button>
                    </div>
                </div>
            </form>
            <div style="margin-top:15px">
                <?= isset($result) ? $result : '' ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#audience').change(function (e) {
            $audience_filter = $('#audience-filter');
            if (e.target.value == 'filter') {
                $audience_filter.removeClass('hidden');
            } else {
                $audience_filter.addClass('hidden');
            }
        });
    });
</script>
</body>
</html>