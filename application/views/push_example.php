<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter</title>
	<link rel="stylesheet" type="text/css" href="<?= base_url('/css/bootstrap.css') ?>">
    <script type="text/javascript" src="<?= base_url('/js/jquery-3.0.0.js') ?>"></script>
</head>
<body>
<br>
<div id="container" class="container">
    <div class="row">
        <div class="col-sm-2">
            <a class="btn btn-primary btn-block" href="<?= base_url('/') ?>">Push</a>
            <a class="btn btn-primary btn-block" href="<?= base_url('/welcome/push_example') ?>">Push Example</a>
        </div>
        <div class="col-sm-8">
            <form class="form-horizontal" role="form" method="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="">Code</label>
                    <div class="col-sm-10">
                        <pre>
$result = $CI->jpush_library->push()
    ->setPlatform(['android'])
    ->addAllAudience()
    ->setNotificationAlert('Hi, JPush')
    ->addAndroidNotification(
        'Hi, android notification',
        'notification title',
        1,
        ["key1"=>"value1", "key2"=>"value2"]
        )
    ->setMessage(
        "msg content",
        'msg title',
        'type',
        ["key1"=>"value1", "key2"=>"value2"]
    )
    ->send();
                        </pre>
                    </div>
                </div>
                <input type="hidden" name="example" value="example">
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button class="btn btn-success" type="submit">&nbsp;&nbsp; Push &nbsp;&nbsp;</button>
                    </div>
                </div>
            </form>
            <div style="margin-top:15px">
                <?= isset($result) ? $result : '' ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>