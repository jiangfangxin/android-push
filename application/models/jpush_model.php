<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jpush_model extends CI_Model
{
    private $_platforms = ['ios', 'android', 'winphone'];

    public function test_push($post)
    {
        $notification    = isset($post['notification'])    ? $post['notification']    : false;
        $platforms       = isset($post['platforms'])       ? $post['platforms']       : false;
        $audience        = isset($post['audience'])        ? $post['audience']        : false;
        $tag             = isset($post['tag'])             ? $post['tag']             : false;
        $alias           = isset($post['alias'])           ? $post['alias']           : false;
        $registration_id = isset($post['registration_id']) ? $post['registration_id'] : false;

        $CI =& get_instance();
        $CI->load->library('jpush_library');
        $push = $CI->jpush_library->push();

        // Notification
        $error = $this->_notification_error($notification);
        if ($error) {
            return $error;
        }
        $push->setNotificationAlert($notification);

        // Platform
        $error = $this->_platforms_error($platforms);
        if ($error) {
            return $error;
        }
        $push->setPlatform($platforms);

        // Audience
        $error = $this->_audience_error($audience);
        if ($error) {
            return $error;
        }
        switch ($audience) {
            case 'all':
                $push->addAllAudience();
                break;
            case 'filter':
                if ($tag !== false && $tag != '') {
                    $tag = explode(',', $tag);
                    $push->addTag($tag);
                }
                if ($alias !== false && $alias != '') {
                    $alias = explode(',', $alias);
                    $push->addAlias($alias);
                }
                if ($registration_id !== false && $registration_id != '') {
                    $registration_id = explode(',', $registration_id);
                    $push->addRegistrationId($registration_id);
                }
                break;
        }

        $push->send();
    }

    private function _notification_error(&$notification)
    {
        if (!$notification) {
            return 'No notification!';
        }
        return false;
    }

    private function _platforms_error(&$platforms)
    {
        if (is_array($platforms)) {

            foreach ($platforms as $platform) {
                if (!in_array($platform, $this->_platforms)) {
                    return "Platform '$platform' not support!'";
                }
            }

            $platforms = array_unique($platforms);

            if (count($platforms) == count($this->_platforms)) {
                $platforms = 'all';
            }

            return false;
        } else {
            return 'Platform error!';
        }
    }

    private function _audience_error(&$audience)
    {
        if (!$audience) {
            return 'Please choose audience!';
        }

        if (!in_array($audience, ['all', 'filter'])) {
            return 'Please choose right audience!';
        }

        return false;
    }

    public function push_example()
    {
        $CI =& get_instance();
        $CI->load->library('jpush_library');

        try {
            $result = $CI->jpush_library->push()
                ->setPlatform(['android'])
                ->addAllAudience()
                ->setNotificationAlert('Hi, JPush')
                ->addAndroidNotification('Hi, android notification', 'notification title', 1, ["key1"=>"value1", "key2"=>"value2"])
                ->setMessage("msg content", 'msg title', 'type', ["key1"=>"value1", "key2"=>"value2"])
                ->send();
            $result = 'Success!';
        } catch (Exception $e) {
            $result = $e->getMessage();
        }

        return $result;
    }
}
