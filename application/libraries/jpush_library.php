<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . 'third_party/JPush/JPush.php';

class Jpush_library
{
    /**
     * @var JPush 极光推送PHP接口类的实例
     */
    private $_client;

	public function __construct($config)
	{
        $this->_client = new JPush(
            $config['app_key'],
            $config['master_secret'],
            $config['log_file']
        );
	}

    public function push()
    {
        return $this->_client->push();
    }
    
    public function report()
    {
        return $this->_client->report();
    }

    public function device()
    {
        return $this->_client->device();
    }

    public function schedule()
    {
        return $this->_client->schedule();
    }
}
// END Log Class

/* End of file jpush_library.php */
/* Location: ./application/libraries/jpush_library.php */